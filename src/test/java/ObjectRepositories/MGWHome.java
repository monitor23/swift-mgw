package ObjectRepositories;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class MGWHome {
	WebDriver driver;
	WebDriverWait wait;

	@SuppressWarnings("deprecation")
	public MGWHome(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(driver, 300);
		PageFactory.initElements(driver, this);
	}

	public void TestQuery(String query,String ip) throws InterruptedException {
		
        System.out.println(ip+" Login Successful");
		driver.switchTo().frame(wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(
				"html > frameset:nth-child(2) > frameset:nth-child(2) > frameset:nth-child(2) > frame:nth-child(1)"))));
		Thread.sleep(2000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("sql"))).click();
		Thread.sleep(2000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("sql")))
				.sendKeys(query);
		Thread.sleep(2000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("input.button:nth-child(1)"))).click();
		Thread.sleep(2000);
		driver.switchTo().defaultContent();

	}

	public void Disconnect() throws InterruptedException {
		Thread.sleep(3000);
		driver.switchTo().frame(wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.cssSelector("html > frameset:nth-child(2) > frame:nth-child(1)"))));
		wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.cssSelector("td.toolbar:nth-child(1) > a:nth-child(1) > img:nth-child(1)"))).click();
		driver.switchTo().defaultContent();
		Thread.sleep(2000);
		driver.close();
	}

}
