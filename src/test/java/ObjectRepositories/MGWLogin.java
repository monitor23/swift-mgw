package ObjectRepositories;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class MGWLogin {
	WebDriver driver;
	WebDriverWait wait;

	@SuppressWarnings("deprecation")
	public MGWLogin(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(driver, 300);
		PageFactory.initElements(driver, this);
	}

	@FindBy(css = "tr.login:nth-child(8) > td:nth-child(2) > input:nth-child(1)")
	WebElement username;
	@FindBy(css = "tr.login:nth-child(9) > td:nth-child(2) > input:nth-child(1)")
	WebElement password;
	@FindBy(css = "input.button:nth-child(1)")
	WebElement connect;
	@FindBy(css = "tr.login:nth-child(10) > td:nth-child(2) > input:nth-child(2)")
	WebElement testconnection;

	public void WaitFunction() {

		wait.until(ExpectedConditions.visibilityOf(username));

	}

	public WebElement Username() {

		return username;
	}

	public WebElement Password() {
		return password;
	}

	public WebElement Connect() {
		return connect;
	}

	public WebElement TestConnection() {
		return testconnection;
	}

	public void TestConnectionStatus(String ip) throws InterruptedException {
		String status = wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".success"))).getText();
		System.out.println(ip+" Connection Status : "+status);
		Thread.sleep(2000);
		driver.close();
	}

}
