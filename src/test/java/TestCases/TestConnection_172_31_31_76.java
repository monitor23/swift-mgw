package TestCases;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import ObjectRepositories.MGWLogin;

public class TestConnection_172_31_31_76 {
	WebDriver driver;

	@BeforeTest
	public void Initializtion() {
		System.setProperty("webdriver.gecko.driver", "C:\\MyWorkspace\\geckodriver.exe");
		System.setProperty(FirefoxDriver.SystemProperty.DRIVER_USE_MARIONETTE, "true");
		System.setProperty(FirefoxDriver.SystemProperty.BROWSER_LOGFILE, "C:\\SeleniumLogs\\seleniumlogs.txt");
		driver = new FirefoxDriver();
		driver.manage().window().maximize();
		driver.get("https://172.31.31.76:9003/mgw-config-console/login.jsp?");

	}

	@Test
	public void TestConnection76() throws InterruptedException {
		MGWLogin mgwl = new MGWLogin(driver);
		mgwl.WaitFunction();
		mgwl.Username().clear();
		mgwl.Username().sendKeys("mgw");
		mgwl.Password().sendKeys("mgw mgw");
		mgwl.TestConnection().click();
		mgwl.TestConnectionStatus("172.31.31.76");
	}
}
